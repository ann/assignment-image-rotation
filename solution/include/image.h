#ifndef UNTITLEDAKG_IMAGE_H
#define UNTITLEDAKG_IMAGE_H
#include <stdint.h>
struct pixel { uint8_t b, g, r; };
typedef struct pixel pixel;

struct image{
    uint64_t width, height;
    pixel* data;
};

struct image* create_image(uint64_t width, uint64_t height, pixel* data);
void destroy_image(struct image* i);
#endif //UNTITLEDAKG_IMAGE_H
