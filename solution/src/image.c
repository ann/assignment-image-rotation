#include <stdint.h>
#include <malloc.h>
#include "../include/image.h"

struct image* create_image(uint64_t width, uint64_t height, pixel* data){
    struct image* img = malloc(sizeof(struct image));
    img->width = width;
    img->height = height;
    img->data = data;
    return img;
}

void destroy_image(struct image* img){
    free(img->data);
    free(img);
}
