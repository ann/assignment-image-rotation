#include <malloc.h>
#include <stdint.h>
#include "../include/image.h"
#include "../include/transform.h"

pixel ** create_pixels(uint64_t height, uint64_t width){
    pixel** p = malloc(sizeof(pixel*) * height);
    for (size_t i = 0; i < height; i++)
        p[i] = malloc(sizeof(pixel) * width);
    return p;
}

void destroy_pixels(uint64_t height, pixel ** p){
    for (uint64_t i = 0; i < height; i++) free(p[i]);
    free(p);
}

pixel ** get_pixels(struct image const source ){
    pixel ** p = create_pixels(source.height, source.width);
   const size_t size = source.height * source.width;
    for (size_t i = 0; i < size; i++)
        p[i / source.width][i % source.width] = source.data[i];
    return p;
}

struct image* set_pixels(uint64_t height, uint64_t width, pixel** p){
    const uint64_t size = height * width;
    pixel * px = malloc(sizeof(pixel) * size);
    for (size_t i = 0; i < size; i++)
        px[i] = p[i / width][i % width];
    return create_image(width, height, px);
}

pixel ** rotate90(uint64_t height, uint64_t width, pixel** pixels){
    pixel ** p = create_pixels(width, height);
    for (size_t i = 0; i < width; i++){
        for (size_t j = 0; j < height; j++){
            p[i][j] = pixels[height -  1 - j][i];
        }
    }
    return p;
}

struct image rotate(struct image const source ){
    pixel  ** p = get_pixels(source);
    pixel ** px = rotate90(source.height, source.width, p);
    struct image * img = set_pixels(source.width, source.height, px);
    destroy_pixels(source.height, p);
    destroy_pixels(source.width, px);
    return *img;
}