#include "../include/image.h"
#include "../include/input.h"

int32_t get_padding(uint64_t width){
    return (width * 3) % 4 != 0 ? 4 - (width * 3) % 4 : 0;
}

enum write_status write_header90(FILE* out, struct bmp_header* h){
    struct bmp_header header = *h;
    header.biWidth = h->biHeight;
    header.biHeight = h->biWidth;
    header.biXPelsPerMeter = h->biYPelsPerMeter;
    header.biYPelsPerMeter = h->biXPelsPerMeter;
    int32_t pad = get_padding(h->biWidth);
    header.biSizeImage = header.biHeight * (header.biWidth * 3 + pad);
    header.bfileSize = header.bOffBits + header.biSizeImage;
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) return WRITE_ERROR;
    return WRITE_OK;
}

enum read_status read_header(FILE* in, struct bmp_header* header){
    if (fread( header, sizeof( struct bmp_header ), 1, in)){
        if (header->biBitCount != 24) return READ_INVALID_SIGNATURE;
    } else return READ_INVALID_HEADER;
    return READ_OK;
}

enum read_status from_bmp( FILE* in, struct image* img){
    img->data = malloc(sizeof(pixel) * img->height * img->width);
    int32_t padding = get_padding(img->width);
    for (size_t i = 0; i < img->height; i++){
        if(!fread(img->data + (i * img->width),sizeof(pixel), img->width, in))
            return READ_INVALID_BITS;
        if (padding) fseek(in,padding,SEEK_CUR);
    }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img){
    int32_t padding = get_padding(img->width);
    const uint8_t  t = 0;
    for (size_t i = 0; i < img->height; i++) {
        if (!fwrite(img->data + (i * img->width),
                    sizeof(pixel),img->width, out))return WRITE_ERROR;
        if (padding) if (!fwrite(&t, sizeof(uint8_t),padding, out))return WRITE_ERROR;
    }
    return WRITE_OK;
}