#include <stdio.h>
#include "../include/image.h"
#include "../include/util.h"
#include "../include/input.h"
#include "../include/transform.h"

void usage() {
    fprintf(stderr, "Usage: ./image-transformer <source-image> <transformed-image>\n");
}

int main(int argc, char **argv) {
    if (argc != 3) usage();
    if (argc < 3) _err("Not enough arguments \n" );
    if (argc > 3) _err("Too many arguments \n" );
    struct bmp_header h = { 0 };
    FILE * f = fopen(argv[1], "rb");
    if (!f)
        _err("Reading error \n");
    switch (read_header(f, &h)) {
        case READ_INVALID_HEADER:
            _err("Read header error \n");
        case READ_INVALID_SIGNATURE:
            _err("Read file type error \n");
        default: break;
    }
    struct image * img = create_image(h.biWidth,h.biHeight,NULL);
    if (from_bmp(f, img))
        _err("Read bits error \n");
    fclose(f);
    struct image m = rotate(*img);
    destroy_image(img);
    f = fopen(argv[2], "wb");
    if (!f) {
        _err("Opening file error \n");
    }
    if (write_header90(f, &h))_err("Writing header error \n");
    if (to_bmp(f, &m)) _err("Writing error \n");
    fclose(f);
    return 0;
}